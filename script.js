$(document).ready(function() {
    str = ".dropdown-menu .chat-toggle";
    $(".dropdown-menu").click(
       function(event){
         event.stopPropagation();
     });
    
    $('.textarea-chat').keydown(function() {
        if (event.keyCode == 13) {
            var chatnumber = "." + this.name;
            message = this.value;
            time = new Date();
            // var form="form[name=" + this.name +"]";
            // alert(form);
            //http://api.jquery.com/jQuery.post/
            $.post("sendchat.php", {
                //variables to send 
                message: message,
                time: time.getHours() + ":"  + time.getMinutes() + ":" + time.getSeconds(),
                senderID: "1", //userid
                receiverID: "2" //userid
            } )
            .done(function(data) { $(chatnumber).append("<p>" + "Success" + data + "</p>"); })
            .fail(function(data) { $(chatnumber).append("<p>" + "Error" + data + "</p>"); })
            .always(function(data) { $(chatnumber).append("<p>" + message + "</p>"); 
                $(chatnumber).scrollTop($(chatnumber).height() + 500);});
            this.value = null;
            return false;
         }
    });
});

function newChat(chatNumb, senderID, receiverId){
    //if chat exists, just open that tab
    //else: 
    chatnumber = ".chat" + chatNumb;
    newChatBox = "<li class='dropdown'>\
                  <a href='#' class='dropdown-toggle' data-toggle='dropdown'>\
                     Mark Otto\
                     <span class='label label-info'>(1)</span> <b class='caret'></b>\
                  </a>\
                  <ul class='dropdown-menu'>\
                     <div class='chatbox'>\
                        <h5 class='text-info'>Mark Otto</h5>\
                        <hr /> \
                        <div name='chat1' class='chat chat1'>\
                           <p>Hello Yousef!</p>\
                           <p>Hey Mark! Hows are things?</p>\
                           <p>Things are great! I loved the project for 133. It's super awesome</p>   \
                        </div>\
                        <hr />\
                        <form name='chat1' style='margin: 0'>\
                           <textarea name='chat1' class='textarea-chat'></textarea>\
                        </form>\
                     </div>\
                  </ul>\
               </li>";
               $("#navbar-chat").append(newChatBox);
 }
 
 function upgrade(){
    $.post("upgrade.php", {
	//variables to send
		upg: 1,
		del:0,
		men:0
    } )
    .done(function(data) { alert(data)} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
 }
 
 
 function mentor(){
    $.post("upgrade.php", {
	//variables to send
		upg: 0,
		del:0,
		men:1
    } )
    .done(function(data) { alert(data)} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
 }
 
 
 function del(){
    $.post("upgrade.php", {
	//variables to send
		upg: 0,
		del:1,
		men:0
    } )
    .done(function(data) { alert(data); location.href="signout.php";} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
	
 }

 function getMentors(subject){
    alert('Subject received: ' + subject);
    $.post("search.php", {
        //variables to send 
        subject: subject 
    } )
    .done(function(data) {appendToResults(data);} )
    .fail(function(data) { alert("FAIL:" + data) } )
    .always(function(data) { });
    //alert(localjson);
 }

 function appendToResults(data){
    $('#mentor-rows').html("");
    //loop

    var all="";
    var j = JSON.parse(data);
    for(var i=0;i<j.length;i++){
      first=j[i].firstname;
      last=j[i].lastname;
      exper=j[i].experience;
      email="'"+j[i].email+"'";
      html="<tr><td>" + first + " " + last + "</td><td>"+ exper; 
      html += "</td><td><a href='#' class='btn btn-primary' onclick=\"addMentorship(" + email + ")\">Add Mentor</a></td></tr>";  
      $('#mentor-rows').append(html);
    }
 }

  function addMentorship(email){
    alert('Email address received: ' + email);
    $.post("addmentorship.php", {
        //variables to send 
        email: email 
    } )
    .done(function(data) {alert("Successfully called addmentorship.php");} )
    .fail(function(data) { alert("FAIL:" + data) } )
    .always(function(data) { });
    //alert(localjson);
 }