$(document).ready(function() {
    str = ".dropdown-menu .chat-toggle";
    $(".dropdown-menu").click(
       function(event){
         event.stopPropagation();
     });
});


function sendChat(e, obj) {
    if (e.keyCode == 13) { //if they clicked enter
        chatName = "[data-email='" + obj.getAttribute("data-email") + "']";  //AKA Receiver's email
        message = obj.value;
        time = new Date();
        $.post("sendChat.php", {
            //variables to send 
            message: message,
            time: time.getHours() + ":"  + time.getMinutes() + ":" + time.getSeconds(),
            receiveFromEmail: obj.getAttribute("data-email") //userid
        } )
        //if received data
        .done(function(data) { console.log("sendChat.done() " + data); })
        .fail(function(data) { console.log("sendChat.fail() " + data) })
        .always(function(data) { $(chatName).append("<p class='alert alert-success' style='text-align: right'><b> You </b><br />" + message + "</p>"); 
            $(chatName).scrollTop($(chatName).height() + 500);});
        obj.value = null;
        return false;
     }
}

function newChat(receiveFromEmail, receiveFromName){
  //alert("Mentorshipid ($num) = " + receiveFromEmail + ". Mentorname");
  window.setInterval(function(){
  receiveChatFromSender(receiveFromEmail, receiveFromName);
}, 1000);
    //if chat exists, just open that tab
    //else: 
    newChatBox = "<li class='dropdown'>\
                  <a href='#' class='dropdown-toggle' data-toggle='dropdown'>" + 
                     receiveFromName + 
                     " <span class='label label-info'>(1)</span> <b class='caret'></b>\
                  </a>\
                  <ul class='dropdown-menu'>\
                     <div class='chatbox'>\
                        <h5 class='text-info'>" + receiveFromName + "</h5>\
                        <hr /> \
                        <div data-email='" + receiveFromEmail + "' class='chat'>\
                           <p>Hello Yousef!</p>\
                           <p>Hey Mark! Hows are things?</p>\
                           <p>Things are great! I loved the project for 133. It's super awesome</p>   \
                        </div>\
                        <hr />\
                        <form style='margin: 0'>\
                           <textarea data-email='" + receiveFromEmail + "' onkeydown='sendChat(event, this)' class='textarea-chat'></textarea>\
                        </form>\
                     </div>\
                  </ul>\
               </li>";
               $("#navbar-chat").append(newChatBox);
 }
 
 function receiveChatFromSender(receiveFromEmail, receiveFromName){
  chatName = "[data-email='" + receiveFromEmail + "']";
    $.post("receiveChat.php", {
  //variables to send
    receiveFromName: receiveFromName, //mentorshipid
    receiveFromEmail: receiveFromEmail //fullname
    } )
    .done(function(data) { console.log("done() " + data); $(chatName).append("<p class='alert-info'>" + data + "</p>"); })
    .fail(function(data) { console.log("fail()" + data);/*$(chatName).append("<p>" + "Error" + data + "</p>");*/ })
    .always(function(data) { console.log("always()" + data); $(chatName).scrollTop($(chatName).height() + 20000); });
 }

 function upgrade(){
    $.post("upgrade.php", {
  //variables to send
    upg: 1,
    del:0,
    men:0
    } )
    .done(function(data) { alert(data);setInterval("location.reload()", 1000);} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
 }
 
 
 function mentor(){
    $.post("upgrade.php", {
  //variables to send
    upg: 0,
    del:0,
    men:1
    } )
    .done(function(data) { alert(data + " Your mentorship was approved!");setInterval("location.reload()", 1000);} )
    .fail(function(data) { alert(data + " Something went wrong :(") } )
    .always(function(data) { });
 }
 
 
 function del(){
    $.post("upgrade.php", {
  //variables to send
    upg: 0,
    del:1,
    men:0
    } )
    .done(function(data) { alert(data); location.href="signout.php";} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
  
 }

 function getMentors(subject){
    $.post("search.php", {
        //variables to send 
        subject: subject 
    } )
    .done(function(data) {appendToResults(data);} )
    .fail(function(data) { alert("FAIL:" + data) } )
    .always(function(data) { });
    //alert(localjson);
 }

 function appendToResults(data){
    $('#mentor-rows').html("");
    //loop

    var all="";
    var j = JSON.parse(data);
    for(var i=0;i<j.length;i++){
      first=j[i].firstname;
      last=j[i].lastname;
      exper=j[i].experience;
      email="'"+j[i].email+"'";
      html="<tr><td>" + first + " " + last + "</td><td>"+ exper; 
      html += "</td><td><button class='btn btn-primary btn-small' onclick=\"addMentorship(" + email + "); this.disabled=true\">Add Mentor</button></td></tr>";  
      $('#mentor-rows').append(html);
    }
 }

  function addMentorship(mentorEmail){
    $.post("addmentor.php", {
        //variables to send 
        email: mentorEmail
    } )
    .done(function(data) { alert(data);setInterval("location.reload()", 1000);} )
    .fail(function(data) { alert("FAIL:" + data) } )
    .always(function(data) { });
    //alert(localjson);
 }
 
 
 function cancelmentorship(mentorshipid){
    $.post("cancelmentorship.php", {
  //variables to send
    mshipid: mentorshipid
    } )
    .done(function(data) { alert(data);setInterval("location.reload()", 1000);} )
    .fail(function(data) { alert(data) } )
    .always(function(data) { });
 }