<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
<?php 
session_start();
include 'mentorship.php';
if(!isset($_SESSION['email']) || empty($_SESSION['email']) )
{
print("you are not logged in");
header("Location: http://q1337.com/web/signin.html");
}

include 'topline.php'; 
?>
   <!-- <?php include_once 'session.php'; ?>-->
     <!-- HTML5 shim for IE backwards compatibility -->
      <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->
      
     <title>My Dashboard</title>
     <script>
      $('#modal').modal();
     </script>
   </head>
   <body>
   
      <div class="container">
		<div class="media " >
            <span class="span4 pull-right"><label>New Mentors Available</label>
               <a href="#modal" role="button" class="btn btn-success" data-toggle="modal">Search</a></span>
               <!-- Popup -->
			   
			   <?php if($_SESSION['check']==0)
				{
					?>
               <div id="modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                 <div class="modal-header">
                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                   <h3 id="myModalLabel">Modal header</h3>
                 </div>
                 <div class="modal-body">
                   <label style="display: initial;">Choose Subject: </label>
                  <select id="subject-select" onchange="getMentors($(this).val())">
                     <option>- Choose subject -</option>
                     <option>Calculus</option>
                     <option>Physics</option>
                     <option>Operating System</option>
                     <option>Trigonometry</option>
                  </select>
                   <table class="table">
                                 <thead>
                                   <tr>
                                     <th>Name</th>
                                     <th>Experience</th>
                                     <th>Add Mentor</th>
                                   </tr>
                                 </thead>
                                 <tbody id="mentor-rows">
                                   <tr>
                                     <td>No Subject Chosen</td>
                                     <td>No Subject Chosen</td>
                                     <td>No Subject Chosen</td>
                                   </tr>
                                 </tbody>
                               </table>
                 </div>
           
               </div>
			   <?php } ?>
            <a class="pull-left" href="#">
               <img class="media-object" data-src="holder.js/64x64" src="images.jpg" \>
           </a>
            <div class="media-body span5" style="margin-bottom:20px">
               <h4 class="media-heading">
                  <?php echo "$_SESSION[fullname]";
            ?> </h4>
               <?php echo "SUBJECTS: $_SESSION[subjects]";?> 
               <!-- Nested media object -->
               <div class="media">
               <?php echo "EXPERIENCE: $_SESSION[experience]";?> 
               </div>
            </div>
         </div>


         <legend>My Info</legend>
         <div class="tabbable tabs-left table-striped ">
            <ul class="nav nav-tabs">
               <li class="active"><a href="#lA" data-toggle="tab">Inbox <span class="label label-important">(1)</span></a></li>
               <li><a href="#lB" data-toggle="tab">Profile</a></li>
               <li><a href="#lC" data-toggle="tab">Contacts</a></li>
               <li><a href="#lD" data-toggle="tab">Settings</a></li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active" id="lA">
                  <legend>Messages</legend>
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>Mentor</th>
                           <th>Message</th>
                           <th>Reply</th>
                        </tr>
                     </thead>
                     <tbody>
						<?php 
						$count=0;$aa1=new mentorship();
						$aa1->contacts();
						foreach($_SESSION['array'] as $name): 
						?>
                        <tr class="info">
                          <!-- <td>03/09/2013</td> -->
                           <td><?php print $name; 
						   $num = $_SESSION['array_id'][$count];?></td>
                           <td>This is your Chatlog</td>
                           <td>
                              <button 
                                 class="btn btn-info" 
                                 onclick="<?php echo "newChat('$num', '$name')";?>">
                                 Reply
                              </button>
                           </td>
                        </tr>
						<?php $count++; endforeach; ?>
                                             </tbody>
                  </table>
               </div>
               <div class="tab-pane" id="lB">
                  <legend>Profile <button class="btn" onclick="
                     document.getElementsByName('birthdayd')[0].disabled= false;
					 document.getElementsByName('experienced')[0].disabled = false;
                     document.getElementsByName('subjectsd')[0].disabled = false;

                     ">Edit</button></legend>
                  <form class="form-horizontal" action="profile.php" method="post">
                     <div class="control-group">
                        <label class="control-label" for="inputWarning">Full Name</label>
                        <div class="controls">
                           <input type="text" disabled="true" value="<?php echo "$_SESSION[fullname]"; ?>">
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputError">Birthday</label>
                        <div class="controls">
                           <input type="text" name="birthdayd" disabled  value="<?php echo "$_SESSION[dob]"; ?>">
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputError">Subjects</label>
                        <div class="controls">
                           <input type="text" name="subjectsd" disabled  value="<?php echo "$_SESSION[subjects]"; ?>">
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputError">Account Type: </label>
                        <div class="controls">
                           <button href="#" class="btn" onclick="mentor();this.disabled=true;">Become a mentor</button>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputInfo">Experience</label>
                        <div class="controls">
                           <input type="text" name="experienced" disabled  value="<?php echo "$_SESSION[experience]"; ?>">
                        </div>
                     </div>
                
                     <div class="control-group">
                        <label class="control-label" for="inputSuccess">Upload Picture</label>
                         <div class="controls">
                     <input name="MAX_FILE_SIZE" value="102400" type="hidden">
                  <input name="image" accept="image/jpeg" type="file">

                  
                        </div>
						<div class="form-actions">
                           <input class="btn" type="submit" />
                           <button type="button" class="btn" onclick="
                     document.getElementsByName('birthdayd')[0].disabled= true;
					       document.getElementsByName('experienced')[0].disabled = true;
                     document.getElementsByName('subjectsd')[0].disabled = true;

                     ">Cancel</button>
						   </div>
                         </form>
                     </div>

                  </form>
               </div>
               <div class="tab-pane" id="lC">
                  <legend>Contacts</legend>
                  <table class="table table-striped">
                     <thead>
                        <tr>
                           <th>#</th>
                           <th>Name</th>
                           <th>Subject</th>
                           <th>Contact</th>
                        </tr>
                     </thead>
                     <tbody>
						<?php 
						$count=0;$aa=new mentorship();
						$aa->contacts();
						foreach(array_combine($_SESSION['array'],$_SESSION['array_sub']) as $name=> $sub): 
						?>
                        <tr>
                           <td><?php print $count;  ?></td>
                           <td><?php print $name; ?></td>
                           <td><?php print $sub; 
						   $str = $_SESSION['array_id'][$count];?></td>
                           <td>
                              <div class="btn-group">
                                 <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                 Menu
                                 <span class="caret"></span>
                                 </a>
                                 <ul class="dropdown-menu">
                                    <li><a href="#">Chat</a></li>
                                    <li><a href="#" onclick="<?php echo "cancelmentorship('$str');return false";?>">Cancel Mentorship</a></li>
                                    <li><a href="#">*Appointment*</a></li>
                                 </ul> 
                              </div>
                           </td>
						                <td>
                 <!--             <form name="myform" method="post" action="feedback.php">  -->
                                  1 <input type="radio" name="0" value="1"> 
                                    2 <input type="radio" name="0" value="2"> 
                                    3 <input type="radio" name="0" value="3"> 
                                    4 <input type="radio" name="0" value="4" checked=""> 
                                    5 <input type="radio" name="0" value="5"> 
                  
                                    <br>
                                    <p class="alert alert-success" style="float: right; display:none" id="feedback-submitted">Feedback Updated</p><input onclick="$('#feedback-submitted').css('display','initial');" type="submit" value="Update Rating" class="btn" name="submit">
                                <!--    </form>  -->
                                    </td>
    
                        </tr>
                        <?php $count++; endforeach; ?>
						
                     </tbody>
                  </table>
               </div>
               <div class="tab-pane" id="lD">
                  <legend>Settings</legend>
                  <form class="form-horizontal">
                     <div class="control-group">
                        <label class="control-label" for="inputWarning" >Email</label>
                        <div class="controls">
                           <div class="input-append">
                              <input  id="appendedInputButton" type="text" value="<?php echo "$_SESSION[email]"; ?>">
                              <button class="btn" type="button">Edit</button>
                           </div>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputError">Password</label>
                        <div class="controls">
                           <div class="input-append">
                              <input disabled class="span2" id="appendedInputButton" type="password" value="passwordpassword">
                              <button class="btn" type="button">Edit</button>
                           </div>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputInfo">Membership</label>
                        <div class="controls">
                           <div class="input-append">
                              <input disabled class="span2" id="appendedInputButton" type="text" value="<?php if($_SESSION['premium']==1)
							  {
							  echo "PREMIUM"; 
							  }
							  else
							  {
							  echo "BASIC";
							  }?>">
                              <button class="btn btn-success" type="button" onclick="upgrade();this.disabled=true;">Upgrade</button>
                           </div>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label" for="inputSuccess">Account Status</label>
                        <div class="controls">
                           <div class="input-append">
                              <input disabled class="span2" id="appendedInputButton" type="text" value="Expiring soon">
                            <button class="btn btn-danger" type="button" onclick="del()">Delete account</button>
                           </div>
                        </div>
                        <div class="form-actions">
                           <button type="submit" class="btn btn-primary">Save changes</button>
                           <button type="button" class="btn" >Cancel</button>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
      <div class="navbar">
              <div class="navbar-inner navbar-fixed-bottom">
                <ul id="navbar-chat" class="nav pull-right">
                 
                 <!-- <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Mark Otto
                           <span class="label label-info">(1)</span> <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu cht" role="menu" aria-labelledby="dropdownMenu">
                           
                           <div class="chatbox">
                              <h5 class="text-info">Mark Otto</h5>
                              <hr /> 
                              <div class="chat chat1">
                                 <p>Hello Yousef!</p>
                                 <p>Hey Mark! Hows are things?</p>
                                 <p>Things are great! I loved the project for 133. It's super awesome</p>
                              </div>
                                 <hr /> -->
                                 <!-- <form name="<?php //echo $_SESSION['this_mentor']->get_mentor_id();?>" action="chat_log.php" method="post">
                                    <textarea name="<?php //echo $_SESSION['this_mentor']->get_mentor_id();?>" class="textarea-chat chat1"></textarea>
                                 </form> -->

                                <!--  <form name="chat1" action="chat_log.php" method="post" style="margin: 0">
                                    <textarea name="chat1" onkeydown="sendChat(event, this)" class="textarea-chat chat1"></textarea>
                                 </form>
                           </div>
                        </ul>
                  </li> -->
                  
                </ul>
              </div>
            </div>
            <script type="text/javascript">
                $('.dropdown-menu').click(function(e) {
                    e.stopPropagation();
                });
            </script>
      </div>
   </body>
</html>