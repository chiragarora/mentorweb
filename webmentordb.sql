-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 07, 2013 at 02:59 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `webmentordb`
--
CREATE DATABASE `webmentordb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `webmentordb`;

-- --------------------------------------------------------

--
-- Table structure for table `appt`
--

CREATE TABLE IF NOT EXISTS `appt` (
  `appt_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `mentorship_id` int(11) DEFAULT NULL,
  `g_link` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`appt_id`),
  KEY `fk_appt_mentorship` (`mentorship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `chatlog`
--

CREATE TABLE IF NOT EXISTS `chatlog` (
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `mentorship_id` int(11) NOT NULL DEFAULT '0',
  `chat_msg` blob,
  PRIMARY KEY (`mentorship_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `data` int(5) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`),
  KEY `fk_feedback` (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mentee`
--

CREATE TABLE IF NOT EXISTS `mentee` (
  `mentee_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mentor_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`mentee_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `fk_menteeuserid` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mentee`
--

INSERT INTO `mentee` (`mentee_id`, `user_id`, `mentor_count`) VALUES
(2, 91, 0),
(3, 92, 0),
(6, 96, 0),
(10, 99, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mentor`
--

CREATE TABLE IF NOT EXISTS `mentor` (
  `mentor_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `mentee_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`mentor_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `fk_userid` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `mentor`
--

INSERT INTO `mentor` (`mentor_id`, `user_id`, `mentee_count`) VALUES
(1, 98, 0),
(2, 93, 0),
(4, 89, 0);

-- --------------------------------------------------------

--
-- Table structure for table `mentorship`
--

CREATE TABLE IF NOT EXISTS `mentorship` (
  `mentorship_id` int(11) NOT NULL AUTO_INCREMENT,
  `mentor_id` int(11) DEFAULT NULL,
  `mentee_id` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `subject` varchar(40) NOT NULL,
  PRIMARY KEY (`mentorship_id`),
  KEY `fk_mentee_mentorship` (`mentee_id`),
  KEY `fk_mentor_mentorship` (`mentor_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `mentorship`
--

INSERT INTO `mentorship` (`mentorship_id`, `mentor_id`, `mentee_id`, `start_date`, `subject`) VALUES
(1, 1, 2, '2013-05-06', 'Calculus'),
(4, 2, 2, '2013-05-06', 'Operating System'),
(30, 4, 2, '2013-05-07', 'Physics');

-- --------------------------------------------------------

--
-- Table structure for table `pending_requests`
--

CREATE TABLE IF NOT EXISTS `pending_requests` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `mentor_id` int(11) NOT NULL,
  `mentee_id` int(11) NOT NULL,
  PRIMARY KEY (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `firstname` varchar(10) NOT NULL,
  `lastname` varchar(15) NOT NULL,
  `dob` varchar(10) NOT NULL,
  `email` varchar(40) NOT NULL,
  `password` varchar(15) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `linkedinid` varchar(50) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `premium` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`firstname`, `lastname`, `dob`, `email`, `password`, `experience`, `id`, `linkedinid`, `active`, `premium`) VALUES
('asdf', 'asdf', '2012-10-10', 'asdf', 'asdf', 'asasdads', 89, NULL, 1, 1),
('asda', 'aaaa', '2012-10-10', 'asdf@gmail.com', '11111111', 'saja', 91, NULL, 1, 0),
('chirag', 'arora', '2012-10-10', 'samkarora@gmail.com', '11111111', 'shaisha', 92, NULL, 1, 0),
('chuh', 'sjiaj', '2012-10-10', 'samkaroraahs@gmail.coim', '11111111', 'saas', 93, NULL, 1, 0),
('ashsah', 'sajsha', '2012-10-10', 'aa', 'aa', 'saa', 96, NULL, 1, 0),
('temp', 'arora', '2012-10-10', 'chjuahdfa@ksa.cok', '11111111', 'sajshaiu', 98, NULL, 1, 0),
('yousef', 'yousef', '2012-10-10', 'yousef@gmail.com', '11111111', 'shihaih', 99, NULL, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_subjects`
--

CREATE TABLE IF NOT EXISTS `user_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `subject_1` varchar(20) DEFAULT NULL,
  `subject_2` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_subject` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `user_subjects`
--

INSERT INTO `user_subjects` (`id`, `userid`, `subject`, `subject_1`, `subject_2`) VALUES
(59, 89, 'Physics', 'Trigonometry', 'Calculus'),
(60, 91, 'Physics', 'Trigonometry', 'Operating System'),
(61, 92, 'Physics', 'Trigonometry', 'Operating System'),
(62, 93, 'Physics', 'Trigonometry', 'Operating System'),
(64, 96, 'Physics', 'Trigonometry', 'Calculus'),
(65, 98, 'Calculus', 'Trigonometry', 'Physics'),
(66, 99, 'Physics', 'Trigonometry', 'Calculus');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appt`
--
ALTER TABLE `appt`
  ADD CONSTRAINT `fk_appt_mentorship` FOREIGN KEY (`mentorship_id`) REFERENCES `mentorship` (`mentorship_id`);

--
-- Constraints for table `chatlog`
--
ALTER TABLE `chatlog`
  ADD CONSTRAINT `fk_chatlog` FOREIGN KEY (`mentorship_id`) REFERENCES `mentorship` (`mentorship_id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `fk_feedback` FOREIGN KEY (`userid`) REFERENCES `user` (`id`);

--
-- Constraints for table `mentee`
--
ALTER TABLE `mentee`
  ADD CONSTRAINT `fk_menteeuserid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `mentor`
--
ALTER TABLE `mentor`
  ADD CONSTRAINT `fk_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `mentorship`
--
ALTER TABLE `mentorship`
  ADD CONSTRAINT `fk_mentee_mentorship` FOREIGN KEY (`mentee_id`) REFERENCES `mentee` (`mentee_id`),
  ADD CONSTRAINT `fk_mentor_mentorship` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`);

--
-- Constraints for table `user_subjects`
--
ALTER TABLE `user_subjects`
  ADD CONSTRAINT `fk_subject` FOREIGN KEY (`userid`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
